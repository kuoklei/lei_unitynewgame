﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject shot;
    public Transform shotSpawn;
    public Transform shotSpawn1;
    public Transform shotSpawn2;
    public float fireRate;
    public float delay;
    public float shield;

    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("Fire", delay, fireRate);
    }

    //spawn the bolt
    void Fire()
    {
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        Instantiate(shot, shotSpawn1.position, shotSpawn1.rotation);
        Instantiate(shot, shotSpawn2.position, shotSpawn2.rotation);
    }
}
