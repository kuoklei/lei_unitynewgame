﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDestroyByContent : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    public float shield;
    private GameController gameController;


    private void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameControllerObject == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // set up the triggers with different tag
        if (other.tag == "Boundary")
        {
            return;
        }

        if (other.tag == "Enemy")
        {
            return;
        }

        if (other.tag == "boss")
        {
            return;
        }

        if (other.tag == "Enemy_Bolt")
        {
            return;
        }

        if (explosion != null)
        {
            Instantiate(explosion, transform.position, transform.rotation);
        }

        //trigger on Player
        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
        }
        
        Destroy(other.gameObject);

        //this is counting the shield
        if(shield != 0)
        {
            shield -= 1;
        }
        else
        {
            Destroy(gameObject);
            gameController.AddScore(scoreValue);
        }
    }
}
