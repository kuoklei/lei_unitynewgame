﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public GameObject[] hazards2;
    public GameObject[] hazards3;
    public GameObject[] hazards4;
    public Vector3 spawnValues;
    public Vector3 spawnValues2;
    public Vector3 spawnValues3;
    public int hazardCount;
    public int hazardCount2;
    public int hazardCount3;
    public float spawnWait;
    public float spawnWait2;
    public float startWait;
    public float waveWait;
    public float waveWait3;

    public Text scoreText;
    public Text restartText;
    public Text gameOverText;

    private bool gameOver;
    private bool restart;
    private int score;

    private void Start()
    {
        //variables
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        UpdateScore();
        StartCoroutine (SpawnWaves());
    }

    private void Update()
    {
        // restart button
        if (restart)
        {
            if(Input.GetKeyDown(KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }
    }

    //spawn the enemies in order
    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            //wave 1
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0,hazards.Length)];
                Vector3 spawnPosition = new Vector3(spawnValues.x, Random.Range(- spawnValues.y, spawnValues.y), spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            //wave 2
            for (int i = 0; i < hazardCount2; i++)
            {
                GameObject hazard2 = hazards2[Random.Range(0, hazards2.Length)];
                Vector3 spawnPosition2 = new Vector3(spawnValues2.x, Random.Range(-spawnValues.y, 0), spawnValues2.z);
                Quaternion spawnRotation2 = Quaternion.identity;
                Instantiate(hazard2, spawnPosition2, spawnRotation2);
                Vector3 spawnPosition3 = new Vector3(-spawnValues2.x, Random.Range(0, spawnValues.y), spawnValues2.z);
                Instantiate(hazard2, spawnPosition3, spawnRotation2);
                yield return new WaitForSeconds(spawnWait2);
            }
            yield return new WaitForSeconds(waveWait);

            //wave 3
            for (int i = 0; i < hazardCount3; i++)
            {
                GameObject hazard3 = hazards3[Random.Range(0, hazards3.Length)];
                GameObject hazard4 = hazards4[Random.Range(0, hazards4.Length)];
                Vector3 spawnPosition4 = new Vector3(spawnValues3.x, Random.Range(-spawnValues3.y, 0), spawnValues3.z);
                Quaternion spawnRotation4 = Quaternion.identity;
                Instantiate(hazard3, spawnPosition4, spawnRotation4);
                Vector3 spawnPosition5 = new Vector3(spawnValues3.x, Random.Range(0, spawnValues3.y), spawnValues3.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard4, spawnPosition5, spawnRotation4);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait3);

            if (gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }

        }

    }

    // Update the Score
    public void AddScore (int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    // Show GameOver
    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }
}
