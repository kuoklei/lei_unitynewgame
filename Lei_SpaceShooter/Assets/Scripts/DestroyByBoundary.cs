﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this is used to destroy object run out of screen 
public class DestroyByBoundary : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        Destroy(other.gameObject);
    }
}
